import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import urllib2
from uuid import uuid4
from bs4 import BeautifulSoup
import html5lib
from school_url import DICT
from django.core.cache import cache
from lib.models import Comment

APPID = "wx5102b7fa0fa2711b"
SECRET = "e4e15e075c5bc3c206faddf12cdc45bb"


def parse_request(base_url, book_name):
    url = "http://%s/opac/search_rss.php?dept=ALL&title=%s&match_flag=any" % (base_url, book_name)
    req = urllib2.Request(url, headers={'User-Agent': "Magic Browser"})
    content = urllib2.urlopen(req).read()
    soup = BeautifulSoup(content, "html.parser")
    tag = soup.channel
    items = tag.find_all("item")
    books = []
    for item in items:
        book = {}
        book['title'] = item.find("title").text
        book['marc_no'] = item.find("link").text.split("=")[1]
        description = {}
        description['press'] = item.find("description").text.split(":")[-1]
        description['author'] = item.find("description").text.split(":")[1][:-4]
        book['description'] = description
        books.append(book)
    count = len(books)
    alot = False
    if count > 200:
        alot = True
        books = books[0:200]
    return books, alot, count


def getRuler(head):
    ruler = {}
    for index, title in enumerate(head):
        if title.text == u'\u7d22\u4e66\u53f7':
            ruler['code'] = index
        if title.text == u'\u9986\u85cf\u5730' or title.text == u'\u6821\u533a\u2014\u9986\u85cf\u5730':
            ruler['place'] = index
        if title.text == u'\u4e66\u520a\u72b6\u6001':
            ruler['status'] = index
    return ruler


def formatPlace(place):
    place = place.replace("\t", "")
    place = place.replace("\r", "")
    place = place.replace("\n", "")
    place = place.replace(" ", "")
    place = place.split("document")[0].split('$')[0]
    return place


def parse_detail_request(base_url, marc_no):
    url = "http://%s/opac/item.php?marc_no=%s" % (base_url, marc_no)
    req = urllib2.Request(url, headers={'User-Agent': "Magic Browser"})
    content = urllib2.urlopen(req).read()
    soup = BeautifulSoup(content, "html5lib")
    tag = soup.body
    items_parse = tag.find('table')
    items_book = items_parse.find_all('tr')
    book_name = tag(class_="booklist")[0].find('dd').text.split("/")[0]
    try:
        book_author = tag(class_="booklist")[0].find('dd').text.split("/")[1]
    except:
        book_author = ''
    try:
        book_press = tag(class_="booklist")[1].find('dd').text
    except:
        book_press = ''
    try:
        douban_link = tag(class_="sharing_zy")[0].find('a').get('href')
        print len(douban_link)
        if 'douban' in douban_link and len(douban_link) > 32:
            has_douban = True
        else:
            has_douban = False
    except:
        has_douban = False

    books = []
    ruler = getRuler(items_book[0].find_all('td'))
    for x in xrange(1, len(items_book)):
        book = {}
        try:
            code = items_book[x].find_all('td')[ruler['code']].text
            book['place'] = formatPlace(items_book[x].find_all('td')[ruler['place']].text)
            book['status'] = items_book[x].find_all('td')[ruler['status']].text
        except:
            code = ''
            book['place'] = ''
            book['status'] = ''
        books.append(book)
    return books, book_name, book_author, book_press, has_douban, code


def get_related_comment(school_code, marc_no, openid):
    comments = Comment.objects.filter(school_code=school_code, marc_no=marc_no)
    related_comments = []
    for comment in comments:
        related_comment = {}
        related_comment['id'] = comment.id
        related_comment['name'] = comment.user_name[0] + '**'
        related_comment['comment'] = comment.comment
        related_comment['datetime'] = str(comment.created_at)[:19]
        print comment.created_at
        if comment.user_openid == openid:
            related_comment['is_mine'] = True
        else:
            related_comment['is_mine'] = False
        related_comments.append(related_comment)
    return related_comments


@csrf_exempt
def api_search(request):
    if request.method == 'POST':
        book_name = request.POST.get("book", '').encode('utf8')
        base_url = DICT[request.POST.get("school", '').encode('utf8')]
        books, alot, book_count = parse_request(base_url, book_name)
        response_data = {}
        response_data['books'] = books
        response_data['is_alot'] = alot
        response_data['book_count'] = book_count
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def api_detail(request):
    if request.method == 'POST':
        marc_no = request.POST.get("marc_no", '').encode('utf8')
        school_code = request.POST.get("school", '').encode('utf8')
        session_id = request.POST.get("session_id", '').encode('utf8')
        openid = cache.get(session_id, "expired")
        if openid != "expired":
            openid = openid.split("==")[1]
        base_url = DICT[school_code]
        books, book_name, book_author, book_press, has_douban, code = parse_detail_request(base_url, marc_no)
        response_data = {}
        response_data['book_name'] = book_name
        response_data['book_author'] = book_author
        response_data['code'] = code
        response_data['books'] = books
        response_data['comments'] = get_related_comment(school_code, marc_no, openid)
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def api_push_comment(request):
    if request.method == 'POST':
        marc_no = request.POST.get("marc_no", '').encode('utf8')
        school_code = request.POST.get("school", '').encode('utf8')
        comment = request.POST.get("comment", '').encode('utf8')
        session_id = request.POST.get("session_id", '').encode('utf8')
        nick_name = request.POST.get("nick_name", '').encode('utf8')
        openid = cache.get(session_id, "expired")
        if openid != "expired":
            openid = openid.split("==")[1]
        Comment.objects.create(school_code=school_code, comment=comment, marc_no=marc_no, user_name=nick_name,
                               user_openid=openid)
        response_data = {}
        response_data['comments'] = get_related_comment(school_code, marc_no, openid)
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def api_login_status(request):
    if request.method == 'POST':
        code = request.POST.get("code", '').encode('utf8')
        url = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code" % (
            APPID, SECRET, code)
        req = urllib2.Request(url)
        content = json.loads(urllib2.urlopen(req).read())
        if 'errcode' not in content:
            session_key = content['session_key'].encode('utf8')
            openid = content['openid'].encode('utf8')
            session_token = session_key + openid
            session_id = str(uuid4()).replace('-', '').upper()
            response_data = {'status': 'success'}
            response_data['session_id'] = session_id
            cache.set(session_id, session_token, int(content['expires_in']))
        else:
            response_data = {'status': 'fail'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def api_check_expired(request):
    if request.method == 'POST':
        session_id = request.POST.get("session_id", '').encode('utf8')
        if cache.get(session_id, '') == '':
            status = 'expired'
        else:
            status = 'available'
        response_data = {'status': status}
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def api_delete_comment(request):
    if request.method == 'POST':
        comment_id = request.POST.get("comment_id", '').encode('utf8')
        session_id = request.POST.get("session_id", '').encode('utf8')
        marc_no = request.POST.get("marc_no", '').encode('utf8')
        school_code = request.POST.get("school", '').encode('utf8')
        openid = cache.get(session_id, "expired")
        if openid != "expired":
            openid = openid.split("==")[1]
        Comment.objects.filter(id=comment_id, user_openid=openid).delete()
        response_data = {}
        response_data['comments'] = get_related_comment(school_code, marc_no, openid)
        return HttpResponse(json.dumps(response_data), content_type="application/json")

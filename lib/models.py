#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

# Create your models here.
class Comment(models.Model):
    school_code = models.CharField(max_length=20,verbose_name="学校代码")
    marc_no = models.CharField(max_length=100,verbose_name="图书代码")
    user_name = models.CharField(max_length=255,verbose_name="用户名")
    user_openid = models.CharField(max_length=255,verbose_name="OPENID")
    comment = models.TextField(verbose_name="评论")
    created_at = models.DateTimeField(auto_now=True)
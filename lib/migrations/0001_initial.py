# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('school_code', models.CharField(max_length=20, verbose_name=b'\xe5\xad\xa6\xe6\xa0\xa1\xe4\xbb\xa3\xe7\xa0\x81')),
                ('marc_no', models.CharField(max_length=100, verbose_name=b'\xe5\x9b\xbe\xe4\xb9\xa6\xe4\xbb\xa3\xe7\xa0\x81')),
                ('user_name', models.CharField(max_length=255, verbose_name=b'\xe7\x94\xa8\xe6\x88\xb7\xe5\x90\x8d')),
                ('user_openid', models.CharField(max_length=255, verbose_name=b'OPENID')),
                ('comment', models.TextField(verbose_name=b'\xe8\xaf\x84\xe8\xae\xba')),
                ('created_at', models.DateField(auto_now=True)),
            ],
        ),
    ]

from django.conf.urls import include, url
from django.contrib import admin

from lib.views import api_search, api_detail, api_login_status, api_check_expired, api_push_comment, api_delete_comment

urlpatterns = [
    url(r'^admin/', include(admin.site.urls))
]

urlpatterns += [
    url('^api/search', api_search),
    url('^api/detail', api_detail),
    url('^api/login_status', api_login_status),
    url('^api/check_expired', api_check_expired),
    url('^api/push_comment', api_push_comment),
    url('^api/delete_comment', api_delete_comment)
]